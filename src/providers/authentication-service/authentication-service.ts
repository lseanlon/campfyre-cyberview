import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class AuthenticationServiceProvider {

  user: Observable<firebase.User>;
  currentUserId: any;
  isLoggedIn: boolean = false;
  public DEFAULT_LANDING_PAGE: string = 'PreloginPage';

  constructor(public router: Router,
    public http: Http, private firebaseAuth: AngularFireAuth) {
    this.user = firebaseAuth.authState;

  }
  setIsLoggedIn(isLoggedIn) {
    this.isLoggedIn = isLoggedIn;
  }
  getIsLoggedIn() {
    return this.isLoggedIn  ;
  }
  signup(email: string, password: string) {
    this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {

        this.isLoggedIn = true;
        console.log('Success!', value);
      })
      .catch(err => {
        console.log('Something went wrong:', err.message);
        alert(err.message);
      });
  }

  login(email: string, password: string) {
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        this.isLoggedIn = true;
        this.router.navigateByUrl('/dashboard');

      })
      .catch(err => {
        alert(err.message);
        console.log('Something went wrong:', err.message);
      });
  }

  googleLogin() {

    const provider = new firebase.auth.GoogleAuthProvider()
    return this.socialSignIn(provider);
  }
  private socialSignIn(provider) {
    return this.firebaseAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        // this.user = credential.user
        this.currentUserId = credential.user.uid;
        console.log('credential', credential);
        this.updateUserData()
      })
      .catch(error => console.log(error));
  }
  private updateUserData(): void {
    // Writes user name and email to realtime db
    // useful if your app displays information about users or for admin features

    // let path = `users/${this.currentUserId}`; // Endpoint on firebase
    // let data = {
    //   email: this.authState.email,
    //   name: this.authState.displayName
    // }

    // this.db.object(path).update(data)
    //   .catch(error => console.log(error));

  }

  logout() {
    this.isLoggedIn = false;
    this.firebaseAuth
      .auth
      .signOut();
  }
}
