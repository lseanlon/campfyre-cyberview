import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from 'angularfire2/database';
@Injectable()
export class FirebaseProvider {


    databaseName: string;
    newGeneratedKey: string;
    constructor(public http: Http, public afd: AngularFireDatabase) {
        console.log('  FirebaseProvider Provider');

        afd.object('items').snapshotChanges().map(action => {
            const $key = action.payload.key;
            const data = { key: $key, value: action.payload.val() };
            console.log('daya', data);
            return data;
        }).subscribe(item => console.log(item.key));


        this.afd.list<any>(`/${this.databaseName}/`).snapshotChanges().subscribe((item) => {
            console.log('list snapshotChanges change', item);
        })
        this.afd.list<any>(`/${this.databaseName}/`).valueChanges().subscribe((item) => {
            console.log('list value change', item);
        })
    }


    getDatabaseName() {
        return this.databaseName;
    }
    setDatabaseName(_databaseName) {
        this.databaseName = _databaseName;
    }
    getItems() {
        return this.afd.list<any>(`/${this.databaseName}/`).valueChanges();
    }

    editItem(itemObjectRow) {
        this.afd.object(`/${this.databaseName}/${itemObjectRow.key}`).set(itemObjectRow)
    }
    addItem(itemObjectRow) {
        this.newGeneratedKey = this.afd.list(`/${this.databaseName}/`).push(itemObjectRow).key;
        itemObjectRow.key = this.newGeneratedKey;
        this.afd.object(`/${this.databaseName}/${this.newGeneratedKey}`).set(itemObjectRow);

    }

    removeItem(id) {
        this.afd.list(`/${this.databaseName}/`).remove(id);
    }
}

