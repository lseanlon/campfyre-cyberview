import { Component, OnInit } from '@angular/core';
import { AuthenticationServiceProvider } from './../../providers/authentication-service/authentication-service';

import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // email = 'admin@campfyrex.com';
  // password = 'abcd1234';
  email;
  password;
  constructor(private firebaseAuth: AngularFireAuth, public router: Router,
    public authService: AuthenticationServiceProvider) {

  }
  ngOnInit() {
    this.handleUserSession()
  }

  signup() {
    this.authService.signup(this.email, this.password);
    this.email = this.password = '';
  }

  login() {
    this.authService.login(this.email, this.password);
    this.email = this.password = '';
  }
  googleLogin() {
    this.authService.googleLogin();
  }

  handleUserSession() {
    this.firebaseAuth.auth.onAuthStateChanged((user) => {
      if (user) {
        this.router.navigateByUrl('/dashboard');
      } else {
        this.logout();
      }
    });
  }
  logout() {
    this.authService.logout();
  }
}
