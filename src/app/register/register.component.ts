import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FirebaseProvider } from '../../providers/firebase/firebase';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  DB_NAME: string = 'DB-REGISTER';
  modelRegister = {
    timestamp: '',
    company: {
      name: '',
      regNo: '',
      blobRegistration: '',
      blobProfile: '',
      establishYear: '',
      date: '',
      facebook: '',
      website: '',

      smartCityTechArea: [],
      businessType: [],

      hasMscStatus: false,
      mscStatusCategory: '',
      mscApprovedDate: '',

      coreActivities: '',
      address: '',
      contact: {
        personName: '',
        phone: '',
        phone2: '',
        fax: '',
        telphone: '',
        address: '',
        blobIdentification: ''
      }
    },

    rentalOption: {
      room: '', roomSpace: '', blobPayment: ''
    }
  }

  constructor(public router: Router,
    public firebaseProvider: FirebaseProvider) {
    this.handleInitialization();
  }
  handleInitialization() {
    this.firebaseProvider.setDatabaseName(this.DB_NAME);

  }

  appendRemoveValueFromCheckbox(list, inputItem) {

    const isChecked = inputItem.srcElement.checked;
    const itemValue = inputItem.srcElement.value;
    if (isChecked) {
      list.push(itemValue);
    } else {
      list = list.filter((item) => {
        return item && item !== itemValue;
      });
    }
    console.log(list);
    return list;

  }
  updateCheckbox(_type, inputItem) {
    let list = [];
    if ('businessType' === _type) {
      list = this.modelRegister.company.businessType || []
      this.modelRegister.company.businessType = this.appendRemoveValueFromCheckbox(list, inputItem);

    }
    if ('smartCity' === _type) {
      list = this.modelRegister.company.smartCityTechArea || []
      this.modelRegister.company.smartCityTechArea = this.appendRemoveValueFromCheckbox(list, inputItem);

    }

  }

  // crud  
  addRecord() {
    this.modelRegister.timestamp = new Date().toUTCString();
    this.firebaseProvider.addItem(this.modelRegister);
    alert('Submitted Sucessfully');

    this.router.navigateByUrl('/booking');
    // console.log('x55xx', this.modelRegister);
  }

  editRecord(item) {
    this.firebaseProvider.editItem(item);
  }
  removeRecord(item) {
    this.firebaseProvider.removeItem(item.key)
  }

  ngOnInit() {
  }

  goHome() {

    this.router.navigateByUrl('/');
  }
}
