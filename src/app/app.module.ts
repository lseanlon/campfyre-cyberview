import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HashLocationStrategy, Location, LocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseProvider } from './../providers/firebase/firebase';
import { AuthenticationServiceProvider } from '../providers/authentication-service/authentication-service';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminLandingComponent } from './admin-landing/admin-landing.component';
import { AppRoutingModule } from './app-routing.module'; import { FormsModule } from '@angular/forms';
// Initialize Firebase
const config = {
  apiKey: "AIzaSyDWAOMv9IbRQXZqD7uo5BY1MY5nliMjNsU",
  authDomain: "lifealert-c6f3b.firebaseapp.com",
  databaseURL: "https://lifealert-c6f3b.firebaseio.com",
  projectId: "lifealert-c6f3b",
  storageBucket: "lifealert-c6f3b.appspot.com",
  messagingSenderId: "1069927657785"
};
@NgModule({
  declarations: [
    AppComponent, AdminLandingComponent, LoginComponent, RegisterComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, FormsModule, HttpModule
    , AngularFireDatabaseModule,

    AngularFireModule.initializeApp(config),
  ], providers: [
    FirebaseProvider, 
    AngularFireAuth,
    AuthenticationServiceProvider],
  bootstrap: [AppComponent]
})
export class AppModule { } 

// Location, { provide: LocationStrategy, useClass: HashLocationStrategy },