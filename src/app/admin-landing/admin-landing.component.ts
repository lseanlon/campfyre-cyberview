import { Component, OnInit, Input } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { AuthenticationServiceProvider } from './../../providers/authentication-service/authentication-service';
import { Router } from '@angular/router';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-admin-landing',
  templateUrl: './admin-landing.component.html',
  styleUrls: ['./admin-landing.component.css']
})
export class AdminLandingComponent implements OnInit {

  itemListObservable: any;
  dataList = [];
  dataListLength;
  isInitialized: boolean = false;
  DB_NAME: string = 'DB-REGISTER';

  initTablePlugin(isReady: boolean) {
    // if (this.isInitialized) {
    //   return false;
    // }
    // $("#table1").dataTable();
    // this.isInitialized = true;
    // return true;
  }



  constructor(
    public router: Router, private firebaseAuth: AngularFireAuth,
    public authService: AuthenticationServiceProvider,
    public firebaseProvider: FirebaseProvider) {
    this.handleInitialization();
  }

  handleInitialization() {
    this.firebaseProvider.setDatabaseName(this.DB_NAME);

    this.itemListObservable = this.firebaseProvider.getItems();

    this.itemListObservable.subscribe((_dataList) => {
      this.dataList = (_dataList);
      console.log('before sorted', this.dataList);
      this.sortListByDate();
      console.log('after sorted', this.dataList);
      this.dataListLength = this.dataList.length - 1;


    })
    return this.firebaseProvider.getItems();
  }

  sortListByDate() {
    this.dataList.sort((a, b) => {
      const key1 = new Date(a.date);
      const key2 = new Date(b.date);

      if (key1 < key2) {
        return -1;
      } else if (key1 == key2) {
        return 0;
      } else {
        return 1;
      }

    });
  }
  ngAfterContentInit() { }
  ngOnInit() {

    this.handleUserSession();

  }

  handleUserSession() {

    this.firebaseAuth.auth.onAuthStateChanged((user) => {
      if (user) {
        // User is signed in. 
      } else {
        // No user is signed in.
        this.logout();
      }
    });
  }

  logout() {
    this.authService.setIsLoggedIn(false)
    this.authService.logout();
    this.router.navigateByUrl('/admin-login');
  }
}
