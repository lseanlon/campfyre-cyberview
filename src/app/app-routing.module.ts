import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AdminLandingComponent } from './admin-landing/admin-landing.component';

const routes: Routes = [
    { path: '', redirectTo: '/admin-login', pathMatch: 'full' },
    { path: 'booking', component: RegisterComponent },
    { path: 'admin-login', component: LoginComponent },
    { path: 'dashboard', component: AdminLandingComponent },
    { path: '**', component: LoginComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

